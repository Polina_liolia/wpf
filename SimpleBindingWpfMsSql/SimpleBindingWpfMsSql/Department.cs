﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleBindingWpfMsSql
{
    public class Department
    {
        private int deptNo = 0;
        private String deptName = String.Empty;
        private String deptLoc = String.Empty;

        public Department() { }

        public Department(int DeptNo, String DeptName, String DeptLoc)
        {
            deptNo = DeptNo;
            deptName = DeptName;
            deptLoc = DeptLoc;
        }

        public int DeptNo
        {
            set { deptNo = value; }
            get { return deptNo; }
        }

        public String DeptName
        {
            set { deptName = value; }
            get { return deptName; }
        }

        public String DeptLoc
        {
            set { deptLoc = value; }
            get { return deptLoc; }
        }


    }

}
