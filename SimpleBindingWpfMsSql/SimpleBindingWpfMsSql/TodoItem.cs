﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleBindingWpfMsSql
{
    public class TodoItem
    {
        public string Title { get; set; }
        public int Completion { get; set; }
    }
}
