﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPFWindowTest
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        //для того, чтобы иметь возможность править разметку
        App()
        {
            InitializeComponent();
            //string[] args = Environment.GetCommandLineArgs();
            //StringBuilder sb = new StringBuilder();
            //foreach (string s in args)
            //{
            //    sb.Append(s);
            //    sb.Append(Environment.NewLine); //переход на новую строку в данной ОС
            //}
            //this.Title += sb.ToString();
        }
        [STAThread] //этот атрибут обязателен, указыват на то, что главный поток приложения должен быть этой ф-цией
        //т.е. эта ф-ция будет вызвана при запуске приложения
        static void Main()
        {
      

        }

    }
}
