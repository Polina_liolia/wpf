﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFWindowTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

        }
        public MainWindow(string[] args)
        {
            InitializeComponent();
            StringBuilder sb = new StringBuilder();
            foreach (string s in args)
            {
                sb.Append(s);
                sb.Append(Environment.NewLine); //переход на новую строку в данной ОС
            }
            this.Title = sb.ToString();
        }
    }
}
