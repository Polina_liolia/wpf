﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphicsWPFTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnDrawRectangle_Click(object sender, RoutedEventArgs e)
        {
            Rectangle originalRectangle = new Rectangle();
            originalRectangle.Width = 50;
            originalRectangle.Height = 25;
            originalRectangle.Fill = Brushes.Black;
            originalRectangle.Opacity = 0.5;
            this.plHolder1.Children.Add(originalRectangle);
        }
        private void btnDrawEllipse_Click(object sender, RoutedEventArgs e)
        {
            Ellipse ellipse = new Ellipse()
            {
                Width = 100,
                Height = 50,
                Fill = Brushes.Aquamarine
            };
            this.plHolder2.Children.Add(ellipse);
        }
        private void btnDrawRoundRect_Click(object sender, RoutedEventArgs e)
        {
            Rectangle originalRectangle = new Rectangle();
            originalRectangle.Width = 50;
            originalRectangle.Height = 25;
            originalRectangle.Fill = Brushes.Black;
            originalRectangle.Opacity = 0.5;
            originalRectangle.RadiusX = 5;
            originalRectangle.RadiusY = 5;
            this.plHolder1.Children.Add(originalRectangle);
        }

        private void btnTransformRectangle_Click(object sender, RoutedEventArgs e)
        {
            RotateTransform rotateTransform1 = new RotateTransform(-45, 50, 50);
            this.rec1.RenderTransform = rotateTransform1;
        }

        private void btnDrawLine_Click(object sender, RoutedEventArgs e)
        {
            Line line = new Line()
            {
                X1 = 0,
                Y1 = 0,
                X2 = 50,
                Y2 = 25,
                Width = 50,
                Height = 25,
                Stroke = Brushes.BlueViolet
            };
            this.plHolder2.Children.Add(line);
        }
    }
}
