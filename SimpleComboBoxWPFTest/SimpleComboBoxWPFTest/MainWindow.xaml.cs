﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleComboBoxWPFTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //если у компонента настраивается источник данных (ItemsSource) и нас не устраивает стандартный вид компонента, 
            //то в разметке используется сочетание <ComboBox.ItemTemplate> и <DataTemplate>

            //binding comboBox items to Colors:
            cmbColors.ItemsSource = typeof(Colors).GetProperties();
            cmbColors.SelectedIndex = 0;
            
        }
    }
}
