﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFRoutedEventsTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int logsID;
        private Logs logs;
        private bool event_handled;
        private string txtValue;
        private string textAdded;
        public MainWindow()
        {
            InitializeComponent();
            txtValue = txtInput.Text;
            logsID = 1;
            event_handled = true;
            textAdded = String.Empty;
            logs = new Logs();
            Logs.EtalonLogs = new Logs()
            {
               new MyLog(1, "TextChanged для TextBox пришло H"),
               new MyLog(2, "TextChanged для Grid пришло H"),
               new MyLog(3, "TextChanged для Window пришло H"),

               new MyLog(4, "TextChanged для TextBox пришло e"),
               new MyLog(5, "TextChanged для Grid пришло e"),
               new MyLog(6, "TextChanged для Window пришло e"),

               new MyLog(7, "TextChanged для TextBox пришло y"),
               new MyLog(8, "TextChanged для Grid пришло y"),
               new MyLog(9, "TextChanged для Window пришло y"),

               new MyLog(10, "TextChanged для TextBox пришло !"),
               new MyLog(11, "TextChanged для Grid пришло !"),
               new MyLog(12, "TextChanged для Window пришло !"),
            };
        }

        private void TextBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            event_handled = false;
            string newText = this.txtInput.Text;
            if (!event_handled)
            {   
                if (newText != txtValue && newText.Length > txtValue.Length)
                {
                    int offset = newText.Length - txtValue.Length;
                    textAdded = newText.Substring(newText.Length - offset);
                }
            }
            logs.Add(new MyLog(logsID++, string.Format("TextChanged для TextBox пришло {0}", textAdded)));
            MessageBox.Show("Событие TextChanged перехвачено компнентом TextBox");
            txtValue = newText;
            e.Handled = event_handled = (bool)RadioButton1.IsChecked;//подавляем дальнейшее "движение" события
        }

        private void Grid_TextChanged(object sender, TextChangedEventArgs e)
        {
            
            string newText = this.txtInput.Text;
            if (!event_handled)
            {
                if (newText != txtValue && newText.Length > txtValue.Length)
                {
                    int offset = newText.Length - txtValue.Length;
                    textAdded = newText.Substring(newText.Length - offset);
                }
            }
            logs.Add(new MyLog(logsID++, string.Format("TextChanged для Grid пришло {0}", textAdded)));
            MessageBox.Show("Событие TextChanged перехвачено компнентом Grid");
            txtValue = newText;
            e.Handled = event_handled = (bool)RadioButton2.IsChecked;
        }

        private void Window_TextChanged(object sender, TextChangedEventArgs e)
        {
            string newText = this.txtInput.Text;
            if (!event_handled)
            {
                if (newText != txtValue && newText.Length > txtValue.Length)
                {
                    int offset = newText.Length - txtValue.Length;
                    textAdded = newText.Substring(newText.Length - offset);
                }
            }
            logs.Add(new MyLog(logsID++, string.Format("TextChanged для Window пришло {0}", textAdded)));
            MessageBox.Show("Событие TextChanged перехвачено компнентом Window");
            txtValue = newText;
            e.Handled = event_handled = (bool)RadioButton3.IsChecked;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (logs.EqualsToEtalon())
                MessageBox.Show("Log is correct", "Congrats!");
            else
            {
                MessageBox.Show("Logs contain some mistake!", "Ooops...");
                StringBuilder sb = new StringBuilder();
                foreach (MyLog log in logs)
                    sb.AppendLine(log.ToString());
                MessageBox.Show(sb.ToString(), "Here are your logs:");
            }
        }
    }
}
