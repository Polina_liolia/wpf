﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFRoutedEventsTest
{
    class MyLog : IComparable, IEquatable<MyLog>
    {
        int my_logs_ID;
        string action;
        

        #region Class properties
        public int My_logs_ID
        {
            get
            {
                return my_logs_ID;
            }

            set
            {
                my_logs_ID = value;
            }
        }

        public string Action
        {
            get
            {
                return action;
            }

            set
            {
                action = value;
            }
        }
        #endregion

        public override string ToString()
        {
            return string.Format("logs ID: {0}\naction: {1}",
                this.My_logs_ID, this.Action);
        }

        #region Constructors
        private MyLog() { }
        public MyLog (int my_logs_ID, string action)
        {
            this.My_logs_ID = my_logs_ID;
            this.Action = action;
        }
        #endregion
       
        #region ICompare, IEquatable
        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            MyLog other = obj as MyLog;
            if (other != null) 
                return this.My_logs_ID.CompareTo(other.My_logs_ID); 
            else 
                throw new ArgumentException("object is not MyLog");
        }
        //for IEquatable interface:
        public bool Equals(MyLog other)
        {
            if (other == null)
                return false;
            if (this.My_logs_ID == other.My_logs_ID && this.Action == other.Action)
                return true;
            else
                return false;
        }
        //to use with System.Object:
        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            MyLog logObj = other as MyLog;
            if (logObj == null)
                return false;
            else
                return Equals(logObj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region Compare operators
        public static bool operator == (MyLog current, MyLog other)
        {
            if ((object)current == null || (object)other == null)
                return Object.Equals(current, other);
            return current.Equals(other);
        }

        public static bool operator !=(MyLog current, MyLog other)
        {
            if ((object)current == null || (object)other == null)
                return Object.Equals(current, other);
            return !current.Equals(other);
        }

        public static bool operator >(MyLog current, MyLog other)
        {
            return current.My_logs_ID > other.My_logs_ID;
        }

        public static bool operator <(MyLog current, MyLog other)
        {
            return current.My_logs_ID > other.My_logs_ID;
        }
        #endregion


    }
}
