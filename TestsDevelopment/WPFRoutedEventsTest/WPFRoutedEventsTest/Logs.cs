﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFRoutedEventsTest
{
    class Logs : IList, IEnumerable
    {
        List<MyLog> logs;
        private static Logs etalonLogs = new Logs();

        internal static Logs EtalonLogs
        {
            get
            {
                return etalonLogs;
            }

            set
            {
                etalonLogs = value;
            }
        }

        public Logs()
        {
            logs = new List<MyLog>();
        }

        public bool EqualsToEtalon()
        {
            bool result = true;
            if (etalonLogs != null && etalonLogs.Count == logs.Count)
            {
                int i = 0;
                foreach (MyLog log in logs)
                {
                    if (log != etalonLogs[i++])
                    {
                        result = false;
                        break;
                    }
                }
            }
            else
                result = false;
            return result;
        }

        public IEnumerator GetEnumerator()
        {
            return logs.GetEnumerator();
        }

        //predicate usage:
        public void printLogs(Predicate<MyLog> predicate)
        {
            foreach (MyLog my_log in logs)
            {
                if (predicate(my_log))
                    Console.WriteLine(my_log);
            }
        }

        #region IList methods for MyLog
        public MyLog this[int index]
        {
            //read only
            get
            {
                return logs[index];
            }
        }

        public int Add(MyLog value)
        {
            logs.Add(value);
            return logs.Count;  //returns index of the added element 
        }

        public int Count
        {
            get
            {
                return logs.Count;
            }
        }

        public bool Contains(MyLog value)
        {
            return logs.Contains(value);
        }

        public bool IsFixedSize
        {
            get
            {
                return false;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        public bool IsSynchronized
        {
            get
            {
                return false;
            }
        }

        public int IndexOf(MyLog value)
        {
            return logs.IndexOf(value);
        }


        public void Insert(int index, MyLog value)
        {
            logs.Insert(index, value);
        }


        public object SyncRoot
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        
        #endregion

        #region IList Methods for System.Object
        object IList.this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }       

        public int Add(object value)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, object value)
        {
            throw new NotImplementedException();
        }


        public bool Contains(object value)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }
  
        public void Remove(object value)
        {
            throw new NotImplementedException();
        }


        public int IndexOf(object value)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }
        #endregion


    }
}
