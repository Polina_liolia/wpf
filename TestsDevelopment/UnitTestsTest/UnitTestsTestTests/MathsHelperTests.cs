﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestsTest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace UnitTestsTest.Tests
{
    [TestClass()]
    public class MathsHelperTests
    {
        [TestMethod()]
        // public void AddTest()
        public void sum_10_and_20_30_returned()
        {
            //1. Готовим тестовые данные
            int A = 10;
            int B = 20;
            int expected = 30;

            //2. Создаём тест
            MathsHelper mh = new MathsHelper();
            int result = mh.Add(A, B);

            //3. Проверяем выполнение, используя механизмы Assert (утверждение)
            Assert.AreEqual(expected, result);

            //4. По умолчанию тест не проходим:
            //Assert.Fail();
        }

        [TestMethod()]
        public void substract_20_and_5_15_returned()
        {
            int A = 20;
            int B = 5;
            int expected = 15;
            MathsHelper mh = new MathsHelper();
            int result = mh.Subtract(A, B);
            Assert.AreEqual(expected, result);

            //Assert.Fail();
        }
    }
}