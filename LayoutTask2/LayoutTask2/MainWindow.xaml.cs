﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LayoutTask2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Grid grid;
        Button[,] btns;
        public MainWindow()
        {
            InitializeComponent();

            #region Grid settings
            grid = new Grid();
            this.AddChild(grid);
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            #endregion

            #region Buttons creating and setting their settings
            btns = new Button[3,3]
            {
                { new Button(), new Button(), new Button() },
                { new Button(), new Button(), new Button() },
                { new Button(), new Button(), new Button() }
            };
           
            for(int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    btns[i, j].Name = string.Format("btn{0}{1}", i + 1, j + 1);
                    btns[i, j].Content = string.Format("{0}{1}", i + 1, j + 1);
                    Grid.SetColumn(btns[i, j], j);
                    Grid.SetRow(btns[i, j], i);
                    grid.Children.Add(btns[i, j]);
                }
            #endregion
        }
    }
}
