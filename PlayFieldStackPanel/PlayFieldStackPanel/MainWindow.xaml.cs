﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlayFieldStackPanel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            SetStackPanelSettings(this.first_row);
            SetStackPanelSettings(this.second_row);
            SetStackPanelSettings(this.third_row);
            this.SizeChanged += MainWindow_SizeChanged;
        }

        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetStackPanelSettings(this.first_row);
            SetStackPanelSettings(this.second_row);
            SetStackPanelSettings(this.third_row);
        }

        private void SetStackPanelSettings(StackPanel sp)
        {
            double h = ((Panel)Application.Current.MainWindow.Content).ActualHeight;
            double w = ((Panel)Application.Current.MainWindow.Content).ActualWidth;
            foreach (var btn in sp.Children)
            {
                if (btn is Button)
                {
                    Button button = btn as Button;
                    if (button != null)
                    {
                        button.Width = w / 3;
                        button.Height = h / 3;
                    }
                }
            }
        }
    }
}
