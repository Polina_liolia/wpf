﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

namespace ComboBoxWPFSelectionTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //binding comboBox items to Colors:
            cmbColors.ItemsSource = typeof(Colors).GetProperties();
            cmbColors.SelectedIndex = 0;
        }

        private void cmbColors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //способ извлечь объект-элемент стандартного перечисления
            Color selectedColor = (Color)(cmbColors.SelectedItem as PropertyInfo)//преобразование при помощи рефлексии
                .GetValue(null, null); //получение значения данного свойства
            this.Background = new SolidColorBrush(selectedColor);
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            if (cmbColors.SelectedIndex > 0)
                cmbColors.SelectedIndex--;
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (cmbColors.SelectedIndex < cmbColors.Items.Count - 1)
                cmbColors.SelectedIndex++;
        }

        private void btnBlue_Click(object sender, RoutedEventArgs e)
        {
            cmbColors.SelectedItem = typeof(Colors).GetProperty("Blue"); //способ извлечь значение свойства перечисления
        }
    }
}
