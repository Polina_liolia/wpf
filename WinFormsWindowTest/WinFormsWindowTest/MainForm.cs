﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormsWindowTest
{
    public partial class MainForm : Form
    {
        string[] args;
        
        public MainForm()
        {
            InitializeComponent();
        }

        public MainForm(string [] args)
        {
            InitializeComponent();
            this.args = args;
            StringBuilder sb = new StringBuilder();
            foreach(string s in args)
            {
                sb.Append(s);
                sb.Append(Environment.NewLine); //переход на новую строку в данной ОС
            }
            this.Text = sb.ToString();
        }
    }
}
