﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WinFormsWindowTest
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if(args.Length != 0) //запускаем графический интерфейс только если нет агрументов командной строки
            {
                Application.Run(new MainForm(args));
            }
            //если аргументы были переданы, запускаем в режиме консольного приложения

            
        }
    }
}
