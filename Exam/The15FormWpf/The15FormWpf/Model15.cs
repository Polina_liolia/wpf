﻿using System;

namespace The15FormWpf
{
    public class Model15
    {
        private int[,] matrix;
        private Random random;

        public int[,] Matrix { get { return  matrix; } }

        public Model15()
        {
            matrix = new int[4, 4];
            random = new Random();
            GenerateMatrix();
        }

        public void GenerateMatrix()
        {
            int[] initial_array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }; 
            do
            {
                ShuffleArray(initial_array); 
            }
            while (isCombinationRelevant(initial_array) == false);
            //filling martix with shuffled data:
            FillInMatrix(initial_array);
        }

        //shuffling array
        private void ShuffleArray(int[] initial_array)
        {
            for (int i = initial_array.Length - 1; i >= 1; i--)
            {
                int j = random.Next(i + 1);
                var temp = initial_array[j];
                initial_array[j] = initial_array[i];
                initial_array[i] = temp;
            }
        }

        //filling in matrix with with array values
        private void FillInMatrix(int[] initial_array)
        {
            if (initial_array.Length != 15)
                return;
            for (int i = 0, n = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    if (i == 3 && j == 3) //last field must be empty
                        Matrix[i, j] = 0;
                    else
                        Matrix[i, j] = initial_array[n++];
                }
        }

        //checking if shuffled combination can be ordered back during game
        private bool isCombinationRelevant(int[] array)
        {
            int count_pairs = 0;
            for (int i = 0; i < array.Length - 1; i++)
                for (int j = 1; j < array.Length; j++)
                {
                    if (array[i] > array[j])
                        count_pairs++;
                }
            count_pairs += 4; //number of row with empty spase
            return count_pairs % 2 == 0 ? true : false;
        }



        public bool IsOrdered()
        {
            for (int i = 0, n = 1; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    if (i == 3 && j == 3) //last field must be empty
                        break;
                    if (Matrix[i, j] != n++)
                        return false;
                }
            return true;
        }

        public bool CanMove(int i_start, int j_start, out int i_new, out int j_new)
        {
            if (i_start > 0 && Matrix[i_start-1, j_start] == 0)
            {
                i_new = i_start -1; //moving up
                j_new = j_start; //vertical moving
                return true;
            }
            else if (i_start < 3 && Matrix[i_start+1, j_start] == 0)
            {
                i_new = i_start + 1; //moving down
                j_new = j_start; //vertical moving
                return true;
            }
            else if (j_start > 0 && Matrix[i_start, j_start-1] == 0)
            {
                i_new = i_start;  //horisontal moving
                j_new = j_start - 1;  //moving left
                return true;
            }
            else if(j_start < 3 && Matrix[i_start, j_start+1] == 0)
            {
                i_new = i_start; //horisontal moving
                j_new = j_start + 1; //moving down
                return true;
            }
            //can't move:
            i_new = -1;
            j_new = -1;
            return false;
        }

        public bool Move(int i_start, int j_start, int i_new, int j_new)
        {
            if(matrix[i_new, j_new] == 0)
            {
                matrix[i_new, j_new] = matrix[i_start, j_start];
                matrix[i_start, j_start] = 0;
                return true;
            }
            return false;
        }
    }
}
