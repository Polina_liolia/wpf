﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyLoggerWpfTestLib;

namespace RegFormWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MyLogger logs;
        private MyLogger etalon;
        public MainWindow()
        {
            InitializeComponent();
            logs = new MyLogger();
            logs.USE_D = true;
            etalon = new MyLogger(true); //etalone mode on
            etalon.USE_D = true;
            FillInEtalonLog();
        }

        private void FillInEtalonLog()
        {
            /*Filling in form, for special symbols use right shift button, type digits using keys in the top of keyboard:
                         * Name: ann
                         * Mail: ann@gmail.com
                         * Password: 123456
                         * */
            etalon.WriteProtocol("KeyDown: A", "txtName");
            etalon.WriteProtocol("KeyDown: N", "txtName");
            etalon.WriteProtocol("KeyDown: N", "txtName");
            etalon.WriteProtocol("KeyDown: A", "txtMail");
            etalon.WriteProtocol("KeyDown: N", "txtMail");
            etalon.WriteProtocol("KeyDown: N", "txtMail");
            etalon.WriteProtocol("KeyDown: RightShift", "txtMail");
            etalon.WriteProtocol("KeyDown: D2", "txtMail");
            etalon.WriteProtocol("KeyDown: G", "txtMail");
            etalon.WriteProtocol("KeyDown: M", "txtMail");
            etalon.WriteProtocol("KeyDown: A", "txtMail");
            etalon.WriteProtocol("KeyDown: I", "txtMail");
            etalon.WriteProtocol("KeyDown: L", "txtMail");
            etalon.WriteProtocol("KeyDown: OemPeriod", "txtMail");
            etalon.WriteProtocol("KeyDown: C", "txtMail");
            etalon.WriteProtocol("KeyDown: O", "txtMail");
            etalon.WriteProtocol("KeyDown: M", "txtMail");
            etalon.WriteProtocol("KeyDown: D1", "txtPassword");
            etalon.WriteProtocol("KeyDown: D2", "txtPassword");
            etalon.WriteProtocol("KeyDown: D3", "txtPassword");
            etalon.WriteProtocol("KeyDown: D4", "txtPassword");
            etalon.WriteProtocol("KeyDown: D5", "txtPassword");
            etalon.WriteProtocol("KeyDown: D6", "txtPassword");
            etalon.WriteProtocol("Form submited", "Button");
        }

        private void txtBoxes_KeyDown(object sender, KeyEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txt = sender as TextBox;
                if (txt != null)
                {
                    logs.WriteProtocol(string.Format("KeyDown: {0}", e.Key.ToString()), txt.Name);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            logs.WriteProtocol("Form submited", "Button");
            //checking if test completed:
            bool test_result = MyLogger.CompareWithEtalon(logs.GetCurrentLogName());
            string test_result_message = test_result ? "Test completed!" : "Test failed";
            MessageBox.Show(test_result_message, "Test result");
        }
    }
}
