﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CesarLib
{
    public class CesarDecoder
    {
        private int step;
        private CesarDecoder() { }
        public CesarDecoder(int step)
        {
            this.step = step;
        }

        public string Decode(string path)
        {
            string decodedText = string.Empty;
            using (FileStream fstream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite))
            {
                byte[] output = new byte[fstream.Length];
                fstream.Read(output, 0, output.Length);
                //декодирование:
                for (int i = 0; i < output.Length; i++)
                    output[i] -= (byte)step;
                //декодируем байты в строку:
                decodedText = Encoding.Default.GetString(output);
                Console.WriteLine(decodedText);
                //перемещаемся в начало файла
                fstream.Seek(0, SeekOrigin.Begin);
                //замещаем шифр текстом:
                fstream.Write(output, 0, output.Length);
            }
            return decodedText;
        }
    }
}
