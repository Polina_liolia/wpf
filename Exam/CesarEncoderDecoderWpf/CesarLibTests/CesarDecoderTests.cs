﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CesarLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CesarLib.Tests
{
    [TestClass()]
    public class CesarDecoderTests
    {
        [TestMethod()]
        public void Decode_in_argentina_manit_negra()
        {
            string path = @"..\..\..\ArgentinaToDecode.txt"; // file contains phrase "Гужирхлрг#пгрлх#рижуг";
            string expected = "Аргентина манит негра";
            CesarDecoder decoder = new CesarDecoder(3);
            string result = decoder.Decode(path);
            Assert.AreEqual(result, expected);
        }
    }
}