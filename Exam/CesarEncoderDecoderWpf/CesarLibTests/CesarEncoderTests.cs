﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CesarLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CesarLib.Tests
{
    [TestClass()]
    public class CesarEncoderTests
    {
        [TestMethod()]
        public void Encode_argentina_manit_negra()
        {
            //1. Готовим тестовые данные
            string path = @"..\..\..\ArgentinaToEncode.txt"; // file contains phrase "Аргентина манит негра";
            string expected = "Гужирхлрг#пгрлх#рижуг";

            //2. Создаём тест
            CesarEncoder encoder = new CesarEncoder(3);
            string result = encoder.Encode(path);

            //3. Проверяем выполнение, используя механизмы Assert (утверждение)
            Assert.AreEqual(expected, result);

            //4. По умолчанию тест не проходим:
            //Assert.Fail();
        }
    }
}