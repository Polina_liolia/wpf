﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyLoggerWpfTestLib;

namespace TicTacToeMVC_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IView
    {
        Button[,] btns;
        TicTacToeController controller;
        private MyLogger testLog;
        private MyLogger etalonLog;

        public MainWindow()
        {
            InitializeComponent();
            controller = new TicTacToeController(this);
            createView();
            //creating a test log:
            testLog = new MyLogger();
            testLog.USE_D = true;

            //creating an etalon log for comparation:
            etalonLog = new MyLogger(true);
            etalonLog.USE_D = true;
            FillInEtalonLog();
        }

        private void FillInEtalonLog()
        {
            etalonLog.WriteProtocol("click", "btn00");
            etalonLog.WriteProtocol("value X", "btn00");
            etalonLog.WriteProtocol("click", "btn01");
            etalonLog.WriteProtocol("value O", "btn01");
            etalonLog.WriteProtocol("click", "btn11");
            etalonLog.WriteProtocol("value X", "btn11");
            etalonLog.WriteProtocol("click", "btn02");
            etalonLog.WriteProtocol("value O", "btn02");
            etalonLog.WriteProtocol("click", "btn22");
            etalonLog.WriteProtocol("value X", "btn22");
            etalonLog.WriteProtocol("game over", "view");
        }

        #region IView
        public void clearField()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    btns[i, j].Content = TicTacToeModel.EmptyValue;
                }
        }

        public void createView()
        {
            btns = new Button[3, 3]
            {
                { btn00, btn01, btn02},
                { btn10, btn11, btn12},
                { btn20, btn21, btn22}
            };
        }

        public void sendGameOver(string msg)
        {
            testLog.WriteProtocol("game over", "view");
            MessageBox.Show(msg, "Результат игры");
            
            //checking if test logs are equal to etalon logs:
            bool testResult = MyLogger.CompareWithEtalon(testLog.GetCurrentLogName());
            string testResultMessage = testResult ? "Test completed" : "Test faled";
            MessageBox.Show(testResultMessage, "Test result");
        }
        #endregion

        private void FindViewItem(Button btn, out int iposition, out int jposition)
        {
            iposition = -1;
            jposition = -1;
            for (int i = 0; i < 3; i++)
            {
                bool f = false;
                for (int j = 0; j < 3; j++)
                {
                    if (btns[i, j] == btn)
                    {
                        iposition = i;
                        jposition = j;
                        f = true;
                        break;
                    }
                }
                if (f) break;
            }
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button)
            {
                Button btn = sender as Button;
                if (btn != null)
                {
                    int iposition = -1;
                    int jposition = -1;
                    FindViewItem(btn, out iposition, out jposition);
                    if (iposition != -1 && jposition != -1)
                    {
                        //logging click event
                        testLog.WriteProtocol("click", string.Format("btn{0}{1}", iposition, jposition));
                        string val = string.Empty;
                        bool result = controller.sendPosition(iposition, jposition, out val);
                        if (result)
                        {  
                            //logging button content changing
                            testLog.WriteProtocol(string.Format("value {0}", val),
                                string.Format("btn{0}{1}", iposition, jposition));
                            btn.Content = val;
                        }
                    }
                } 
            }
        }
    }
}
