﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MyLoggerWpfTestLib
{
    public class MyLogger
    {
        private List<string> logsNames;
        private bool use_C;
        private bool use_D;
        private bool use_F;
        private bool use_E;

        #region Constructor
        public MyLogger(bool etalonMode = false)
        {
            logsNames = new List<string>();
            USE_C = true; //default drive
            EtalonMode = etalonMode;
            if (etalonMode == true)
            {
                logsNames.Add(@"c:\myetalonlog.txt");
                logsNames.Add(@"d:\myetalonlog.txt");
                logsNames.Add(@"e:\myetalonlog.txt");
                logsNames.Add(@"f:\myetalonlog.txt");
                MyLogger.EtalonLogPath = logsNames[0]; //default drive
            }
            else
            {
                logsNames.Add(@"c:\mylog.txt");
                logsNames.Add(@"d:\mylog.txt");
                logsNames.Add(@"e:\mylog.txt");
                logsNames.Add(@"f:\mylog.txt");
            }            
        }
        #endregion

        #region Public properties

        //if true, changing target path to "...\myetalonlog.txt", used to create etalon log
        public bool EtalonMode { get; set; }

        public bool USE_C
        {
            get
            {
                return use_C;
            }
            set
            {
                use_C = value;
                if (value == true)
                {
                    use_D = use_E = use_F = false;
                    if (EtalonMode == true)
                        MyLogger.EtalonLogPath = this["C"];
                }
            }
        }

        public bool USE_D
        {
            get
            {
                return use_D;
            }

            set
            {
                use_D = value;
                if (value == true)
                {
                    use_C = use_E = use_F = false;
                    if (EtalonMode == true)
                        MyLogger.EtalonLogPath = this["D"];
                }
            }
        }

        public bool USE_F
        {
            get
            {
                return use_F;
            }

            set
            {
                use_F = value;
                if (value == true)
                {
                    use_D = use_E = use_C = false;
                    if (EtalonMode == true)
                        MyLogger.EtalonLogPath = this["F"];
                }
            }
        }

        public bool USE_E
        {
            get
            {
                return use_E;
            }

            set
            {
                use_E = value;
                if (value == true)
                {
                    use_D = use_C = use_F = false;
                    if (EtalonMode == true)
                        MyLogger.EtalonLogPath = this["E"];
                }
            }
        }
        #endregion

        #region Indexators
        public string this[int index]
        {
            get { return logsNames[index]; }
        }

        public string this[string value]
        {
            get
            {
                if (value.Equals("C"))
                    return logsNames[0];
                if (value.Equals("D"))
                    return logsNames[1];
                if (value.Equals("E"))
                    return logsNames[2];
                if (value.Equals("F"))
                    return logsNames[3];
                else
                    throw new ArgumentOutOfRangeException("Such value was not found");
            }
        }
        #endregion

        #region WriteProtocol overloads
            //uses current log
        public void  WriteProtocol(string action, string whoCalled)
        {
            string currentLogName = GetCurrentLogName();
            try
            {
                using (StreamWriter sw = new StreamWriter(currentLogName, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(action);
                    sw.WriteLine(whoCalled);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        //uses directly pointed log as string
        public void WriteProtocol(string logName, string action, string whoCalled)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(logName, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(action);
                    sw.WriteLine(whoCalled);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        #endregion

        #region Comparation of logs for unit tests
        //contains a path to etalon log to compare test logs with
        public static string EtalonLogPath { get; set; }
        //compares test log file with etalon log, returns true if equal
        public static bool CompareWithEtalon(string testLogPath)
        {
            bool result = false;
            if (File.Exists(testLogPath) && File.Exists(MyLogger.EtalonLogPath))
            {
                string testLog = String.Empty;
                string etalonLog = String.Empty;
                //reading all test log file
                try
                {
                    using (StreamReader sr = new StreamReader(testLogPath, Encoding.Default, true))
                    {
                        testLog = sr.ReadToEnd();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                //reading all etalon file
                try
                {
                    using (StreamReader sr = new StreamReader(MyLogger.EtalonLogPath, Encoding.Default, true))
                    {
                        etalonLog = sr.ReadToEnd();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                //defining, are two logs equal
                result = testLog.Equals(etalonLog);
            }
            else
            {
                throw new FileNotFoundException("Test log file or Etalon log file does not exist.");
            }
            return result;
        }

        #endregion

        public string GetCurrentLogName()
        {
            return use_F ? this["F"] :
                use_D ? this["D"] :
                use_E ? this["E"] : this["C"];
        }
    }
}
