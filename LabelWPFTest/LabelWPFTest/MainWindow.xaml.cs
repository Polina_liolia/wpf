﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LabelWPFTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("Selection starts at character #{0}{1}", textBox.SelectionStart, Environment.NewLine));
            sb.Append(string.Format("Selection is {0} character(s) long{1}", textBox.SelectionLength, Environment.NewLine));
            sb.Append(string.Format("Selected text: '{0}'{1}", textBox.SelectedText, Environment.NewLine));
            txtStatus.Text = sb.ToString();

        }
    }
}
