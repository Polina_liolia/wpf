﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphicsWpfTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnDrawRectangle_Click(object sender, RoutedEventArgs e)
        {
            Rectangle originalRectangle = new Rectangle();
            originalRectangle.Width = 50;
            originalRectangle.Height = 25;
            originalRectangle.Fill = Brushes.Black;
            originalRectangle.Opacity = 0.5;
            this.plHolder1.Children.Add(originalRectangle);
        }
        private void btnDrawEllipse_Click(object sender, RoutedEventArgs e)
        {
           //ДЗ
        }
        private void btnTransformRectangle_Click(object sender, RoutedEventArgs e)
        {
            RotateTransform rotateTransform1 
                = new RotateTransform(-45, 50, 50);
            this.rec1.RenderTransform = rotateTransform1;
        }
        private void btnDrawRoundRect_Click(object sender, RoutedEventArgs e)
        {
            Rectangle originalRectangle = new Rectangle();
            originalRectangle.Width = 50;
            originalRectangle.Height = 25;
            originalRectangle.Fill = Brushes.Black;
            originalRectangle.Opacity = 0.5;
            originalRectangle.RadiusX = 5;
            originalRectangle.RadiusY = 5;
            originalRectangle.Margin = new Thickness(5);
            this.plHolder1.Children.Add(originalRectangle);
        }
        private void btnDrawLine_Click(object sender, RoutedEventArgs e)
        {
            Line line = new Line();
            line.X1 = 0;
            line.Y1 = 0;
            line.X2 = 50;
            line.Y2 = 25;
            line.Width = 50;
            line.Height = 25;
            line.Stroke = Brushes.BlueViolet;
            line.Stroke = new SolidColorBrush(Color.FromArgb(100, 234, 0, 20));
            this.plHolder2.Children.Add(line);
        }

        private void btnDrawPlygon_Click(object sender, RoutedEventArgs e)
        {
            Polygon poligon = new Polygon();
            poligon.Points.Add(new Point(30, 40));
            poligon.Points.Add(new Point(40, 40));
            poligon.Points.Add(new Point(30, 50));
            poligon.Points.Add(new Point(40, 50));
            poligon.Points.Add(new Point(30, 40));
            poligon.Stroke = Brushes.Black;
            poligon.Fill = Brushes.Black;
            poligon.Width = 100;
            poligon.Height = 50;
            this.plHolder2.Children.Add(poligon);
        }

    }
}
