﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextBlockWPFTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            AddTextBlock();
        }

        private void AddTextBlock()
        {
            TextBlock tb = new TextBlock();
            tb.TextWrapping = TextWrapping.Wrap;
            tb.Margin = new Thickness(10);
            tb.Inlines.Add("An example on ");
            tb.Inlines.Add(new Run("the TextBlock control ") //добавление форматированного эл-та внутрь другого тега
                { FontWeight = FontWeights.Bold });
            this.PlaceHolder.Children.Add(tb);
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
           // MessageBox.Show("HyperLink event occured", "Hyper link event handler");
            System.Diagnostics.Process.Start(e.Uri.AbsoluteUri);
            //если в процесс передается ф-л, для открытия которого есть ассоциированное приложение,
            //то будет запущено это приложение
        }
    }
}
