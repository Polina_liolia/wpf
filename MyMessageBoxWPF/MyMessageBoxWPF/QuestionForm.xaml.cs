﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyMessageBoxWPF
{
    /// <summary>
    /// Interaction logic for QuestionForm.xaml
    /// </summary>
    public partial class QuestionForm : Window
    {
        public string QuestionText { get; set; }
        public bool UsersChoise { get; set; }
        public QuestionForm(string questionText)
        {
            this.QuestionText = questionText;
            InitializeComponent();
            //MessageBox.Show(this.QuestionText); //test
            this.question.Text = QuestionText;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (chb_agree.IsChecked == false)
                MessageBox.Show("Вы не отметили, что согласны с условиями!");
            else
            {
                UsersChoise = rb_yes.IsChecked == true ? true : false;
                this.Close();
            }
        }
    }
}
