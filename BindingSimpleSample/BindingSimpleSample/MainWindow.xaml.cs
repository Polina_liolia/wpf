﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BindingSimpleSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private int counter = 0;
        private DataSet dataset;
        private ObservableCollection<string> list_items;
       
        public MainWindow()
        {
            InitializeComponent();
            sp_WindowResize.DataContext = this;
            sp_ClicksCount.DataContext = this;
            list_items = new ObservableCollection<string>()
            {
                "item1", "item2"
            };
            lb1.ItemsSource = list_items;
            
            GetAdventureWorksDataFromDataBase();
            
            //listView vs DataSet:
            lb_person.ItemsSource = dataset.Tables["Person"].AsDataView();

            //comboBox vs DataSet:
            cb_person.ItemsSource = dataset.Tables["Person"].AsDataView();

            //dataGrid vs DataSet:
            dg_person.ItemsSource = dataset.Tables["Person"].AsDataView();


        }

        public int Counter
        {
            get { return this.counter; }
            set
            {
                counter = value;
                NotifyPropertyChanged("Counter");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Counter++;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            list_items.Add(string.Format("item{0}", list_items.Count + 1));
        }

        private void GetAdventureWorksDataFromDataBase()
        {
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["MSSQL_adv_home"].ConnectionString;
                SqlConnection connection =
                    new SqlConnection(connectionString);
                connection.Open();

                // Create a DataSet.
                this.dataset = new DataSet();
                dataset.Locale = System.Globalization.CultureInfo.InvariantCulture;

                // Add data from the Person table to the DataSet.
                SqlDataAdapter masterDataAdapter = new
                    SqlDataAdapter("select top 10 * from Person.Person", connection);
                masterDataAdapter.Fill(dataset, "Person");
            }
            catch (SqlException ex)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system. : " + ex.ToString());
            }
        }
    }
}
