﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace FileDialogWPFTest
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();
            MainWindow wnd = new MainWindow(args);
            if (e.Args.Length == 1)
                MessageBox.Show(string.Format("Args: {0]", e.Args[0]));
            wnd.Show();
        }
        
    }

   
}
