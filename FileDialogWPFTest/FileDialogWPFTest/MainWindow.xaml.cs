﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FileDialogWPFTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public MainWindow(string [] args)
        {
            InitializeComponent();
            string path = args[args.Length - 1];
            MessageBox.Show(string.Format("Запущено {0}", path));
            //btnDecode.Content = string.Format("Заши-{0}фровать", Environment.NewLine); //как альтернатива &#x0a;
        }

        private void btnOpenFDlg_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                string fullname = openFileDialog.FileName;
                txtFileNameODlg.Text = fullname;
                if (File.Exists(fullname))
                    txtOrigTxt.Text = File.ReadAllText(fullname);
            }
        }

        private void btnSaveFDlg_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == true)
            {
                string fullName = saveFileDialog.FileName;
                txtFileNameSDlg.Text = fullName;
                if (String.IsNullOrEmpty(txtDecodeTxt.Text) == false)
                {
                    File.WriteAllText(fullName, txtDecodeTxt.Text);
                }

            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow about = new AboutWindow();
            about.ShowDialog();//модальное окно
           //about.Show();//НЕмодальное окно
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SimpleEffectsWindow sew = new SimpleEffectsWindow();
            sew.Show();
            
        }
    }
}
