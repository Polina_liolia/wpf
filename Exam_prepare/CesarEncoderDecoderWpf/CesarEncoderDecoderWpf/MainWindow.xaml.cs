﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CesarEncoderDecoderWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnOpenFDlg_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button btn)
            {
                TextBox path = btn == btnOpenFDlgEncode ? txtFileNameDlgEncode : txtFileNameDlgDecode;
                TextBox fileText = btn == btnOpenFDlgEncode ? txtOrigTxt : txtOrigEncoded;
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    string fullname = openFileDialog.FileName;
                    path.Text = fullname;
                    if (File.Exists(fullname))
                        fileText.Text = File.ReadAllText(fullname, Encoding.Default);
                }
            }
            
            
        }


        private void btnEncodeDecode_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button btn)
            {
                string path = btn == btnEncode ? txtFileNameDlgEncode.Text : txtFileNameDlgDecode.Text;
                TextBox result = btn == btnEncode ? txtEncoded : txtDecoded;
                if (File.Exists(path))
                {
                    if (btn == btnEncode)
                    {
                        CesarEncoder encoder = new CesarEncoder(3);
                        encoder.Encode(path);
                    }
                    else if (btn == btnDecode)
                    {
                        CesarDecoder decoder = new CesarDecoder(3);
                        decoder.Decode(path);
                    }
                    else return;
                    result.Text = File.ReadAllText(path, Encoding.Default);
                }
                else
                {
                    MessageBox.Show("No file fount!", "Invalid path", MessageBoxButton.OK, MessageBoxImage.Stop);
                }
            }
        }

       
    }
}
