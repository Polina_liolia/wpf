﻿namespace TicTacToeMVC_WPF
{
    public interface IView
    {
        void sendGameOver(string msg);
        void clearField();
        void createView();
    }
}