﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TicTacToeMVC_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IView
    {
        Button[,] btns;
        TicTacToeController controller;

        public MainWindow()
        {
            InitializeComponent();
            controller = new TicTacToeController(this);
            createView();
        }

        #region IView
        public void clearField()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    btns[i, j].Content = TicTacToeModel.EmptyValue;
                }
        }

        public void createView()
        {
            btns = new Button[3, 3]
            {
                { btn00, btn01, btn02},
                { btn10, btn11, btn12},
                { btn20, btn21, btn22}
            };
        }

        public void sendGameOver(string msg)
        {
             MessageBox.Show(msg, "Результат игры");
        }
        #endregion

        private void FindViewItem(Button btn, out int iposition, out int jposition)
        {
            iposition = -1;
            jposition = -1;
            for (int i = 0; i < 3; i++)
            {
                bool f = false;
                for (int j = 0; j < 3; j++)
                {
                    if (btns[i, j] == btn)
                    {
                        iposition = i;
                        jposition = j;
                        f = true;
                        break;
                    }
                }
                if (f) break;
            }
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button)
            {
                if (sender is Button btn)
                {
                    FindViewItem(btn, out int iposition, out int jposition);
                    if (iposition != -1 && jposition != -1)
                    {
                        bool result = controller.sendPosition(iposition, jposition, out string val);
                        if (result)
                        {
                            btn.Content = val;
                        }
                    }
                }
            }
        }
    }
}
