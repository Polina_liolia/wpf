﻿namespace The15FormWpf
{
    public interface IView
    {
        void Create_view(int [,] matrix);
        void GameOver();
        void Move(int i_start, int j_start, int i_new, int j_new);
        void SendCantMove(int i, int j);
    }
}
