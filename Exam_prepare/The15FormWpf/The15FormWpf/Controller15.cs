﻿

namespace The15FormWpf
{
    public class Controller15
    {
        Model15 model;
        IView view;
        public Controller15(IView view)
        {
            model = new Model15();
            this.view = view;
        }

        public int[,] GetMatrix()
        {
            return model.Matrix;
        }

        public bool CanMove(int i_start, int j_start)
        {
            int i, j;
            bool result = model.CanMove(i_start, j_start, out i, out j);
            if (result)
            {
                model.Move(i_start, j_start, i, j);
                view.Move(i_start, j_start, i, j);
                if (model.IsOrdered())
                {
                    view.GameOver();
                    model.GenerateMatrix();
                    view.Create_view(model.Matrix);
                }
            }
            else
                view.SendCantMove(i_start, j_start);
            return result;
        }



    }
}
