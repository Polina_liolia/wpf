﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace The15FormWpf 
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IView
    {
        Controller15 controller;
        private TextBlock[,] txts;
        public MainWindow()
        {
            InitializeComponent();
            txts = new TextBlock[,]
            {
                { txt00, txt01, txt02, txt03 },
                { txt10, txt11, txt12, txt13 },
                { txt20, txt21, txt22, txt23 },
                { txt30, txt31, txt32, txt33 }
            };
            controller = new Controller15(this);
            Create_view(controller.GetMatrix());
        }

        private void txt_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is TextBlock)
            {
                TextBlock txt = sender as TextBlock;
                if (txt!=null)
                {
                    int i, j;
                    bool result = GetTxtPosition(txt, out i, out j);
                    if (result)
                    {
                        bool moved = controller.CanMove(i, j);
                    }
                }
            }
            
        }

        private bool GetTxtPosition(TextBlock txt, out int pos_i, out int pos_j)
        {
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    if (txt == txts[i,j])
                    {
                        pos_i = i;
                        pos_j = j;
                        return true;
                    }
                }
            pos_i = -1;
            pos_j = -1;
            return false;
        }

        #region IView
        public void Create_view(int[,] matrix)
        {
            if(matrix.Length != txts.Length)
            {
                throw new ArgumentException("Can't build play field: wrong base matrix size.");
            }
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    if (matrix[i, j] == 0)
                        continue;
                    txts[i, j].Text = matrix[i, j].ToString();
                }
        }

        public void GameOver()
        {
            MessageBox.Show("Game over!", "Congratulations!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        public void Move(int i_start, int j_start, int i_new, int j_new)
        {
            string tmp = txts[i_new, j_new].Text;
            txts[i_new, j_new].Text = txts[i_start, j_start].Text;
            txts[i_start, j_start].Text = tmp;
        }

        public void SendCantMove(int i, int j)
        {
            txts[i, j].Background = new SolidColorBrush(Colors.White); //'defrozing' background property
            ColorAnimation animation;
            animation = new ColorAnimation
            {
                From = Colors.White,
                To = Colors.Red,
                Duration = new Duration(TimeSpan.FromSeconds(1))
            };
            txts[i,j].Background.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            animation = new ColorAnimation
            {
                From = Colors.Red,
                To = Colors.White,
                Duration = new Duration(TimeSpan.FromSeconds(1))
            };
            txts[i, j].Background.BeginAnimation(SolidColorBrush.ColorProperty, animation);
        }
        #endregion

        
    }
}
