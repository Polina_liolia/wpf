﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TriangleRotateTransformWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            Storyboard sb = (Storyboard)TryFindResource("sb_triangle3");
            if (sb != null)
                sb.Begin();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            Storyboard sb = (Storyboard)TryFindResource("sb_triangle3");
            if (sb != null)
                sb.Stop();
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            Storyboard sb = (Storyboard)TryFindResource("sb_triangle3");
            if (sb != null)
                sb.Pause();
        }

        private void btnResume_Click(object sender, RoutedEventArgs e)
        {
            Storyboard sb = (Storyboard)TryFindResource("sb_triangle3");
            if (sb != null)
                sb.Resume();
        }
    }
}
