﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace WPFWindowsTest
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        App()
        {
            InitializeComponent();
            string[] args = Environment.GetCommandLineArgs();
            foreach (string s in args)
            {
                //this.Text += s + Environment.NewLine;
            }
        }
    }
}
